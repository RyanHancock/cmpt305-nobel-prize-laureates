package nobellaureates;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Holds all data for a Laureate, including prizes and affiliations
 * Implements comparable, allowing for easy sorting of Laureate lists
 * Provides interface for public access of its fields
 * @author Ryan Hancock
 */
public class Laureate implements Comparable {
    private String id;
    private String born;
    private String bornCountry;
    private String died;
    private String diedCountryCode;
    private String diedCity;
    private String gender;
    private String bornCountryCode;
    private String surname;
    private String firstname;
    private String bornCity;
    private String diedCountry;
    private int Age;
    private ArrayList<Affiliations> Affiliates = new ArrayList<>();
    private ArrayList<Prize> Prizes= new ArrayList<>();
    private List<String> info = new ArrayList<>();
    
    
    public ArrayList<Prize> getPrizes(){
        return Prizes;
    }
    
    /**
     * Adds all info fields to this Laureate's info list
     **/
    public List<String> getInfo() {
        info.add(getBorn());
        info.add(getBornCity());
        info.add(getBornCountry());
        info.add((getBornCountryCode()));
        info.add(getDied());
        info.add(getDiedCountry());
        info.add(getDiedCountryCode());
        info.add(getFirstname());
        info.add(getGender());
        info.add(getId());
        info.add(getSurname());
        return info;
    }
    
    @Override
    public int compareTo(Object other) {
        Laureate comp;
        if (other instanceof Laureate) {
            comp = (Laureate) other;
        }
        else {
            throw new IllegalArgumentException();
        }
        
        return this.getFullName().compareTo(comp.getFullName());
    }
    
    public static Comparator <Laureate> lauNameComp = new Comparator <Laureate>() {
        @Override
        public int compare(Laureate lau1, Laureate lau2) {
            String lauName1 = lau1.getFullName();
            String lauName2 = lau2.getFullName();
            return lauName1.compareTo(lauName2);
        }
    };
    
    /**
     * Adds a new prize to this Laureate's prize list
     * @param prize Prize object to be added to this laureate's list of prizes
     **/
    public void addPrize(Prize prize){
        Prizes.add(prize);
    }
    
    
    /**
     * Add a new affiliate to this Laureate's affiliates list
     * @param aff Affiliate object to be added to this laureate's list
     * of affiliates
     **/
    public void addAffiliates(Affiliations aff) {
        Affiliates.add(aff);
    }
    
    // Get Methods
    public ArrayList<Affiliations> getAffiliates() { return Affiliates; } 
    public String getId () { return id; }
    public String getBorn () { return born; }
    public String getBornCountry () { return bornCountry; }
    public String getDied () { return died; }
    public String getDiedCountryCode () { return diedCountryCode; }
    public String getDiedCity () { return diedCity; }
    public String getGender () { return gender; }
    public String getBornCountryCode () { return bornCountryCode; }
    public String getSurname () { return surname; }
    public String getFirstname () { return firstname; }
    public String getBornCity () { return bornCity; }
    public String getDiedCountry () { return diedCountry; }
    public String getFullName() { return this.firstname + " " + this.surname; }
    public int getAge() { return Age; }
    
    // Set Methods
    public void setBorn (String born) { this.born = born; }
    public void setId (String id) { this.id = id; }
    public void setBornCountry (String bornCountry) { this.bornCountry = bornCountry; } 
    public void setDied (String died) { this.died = died; }
    public void setDiedCountryCode (String diedCountryCode) { this.diedCountryCode = diedCountryCode; }
    public void setDiedCity (String diedCity) { this.diedCity = diedCity; }
    public void setGender (String gender) { this.gender = gender; } 
    public void setBornCountryCode (String bornCountryCode) { this.bornCountryCode = bornCountryCode; } 
    public void setSurname (String surname) { this.surname = surname; }
    public void setFirstname (String firstname) { this.firstname = firstname; }
    public void setBornCity (String bornCity) { this.bornCity = bornCity; }
    public void setDiedCountry (String diedCountry) { this.diedCountry = diedCountry; }
    public void setAge(int Age) { this.Age = Age; } 
    
    @Override
    public String toString()
    {
        String nameVal;
        String diedVal, diedCityVal,diedCountryVal,diedCountryCodeVal;
        String bornVal, bornCityVal, bornCountryVal,bornCountryCodeVal;
        String ageVal;
        
        if(surname == null){nameVal = firstname;}else{nameVal = firstname+" "+surname;}
        
        if(Age==0){ageVal = "N/A";}else{ageVal = Integer.toString(Age);}
        
        if(died.equals("0000-00-00")){diedVal = "N/A";}else{diedVal = died;}
        if (diedCity == null || diedCountry == null || diedCountryCode == null){
            diedCityVal= "N/A";diedCountryVal="N/A";diedCountryCodeVal="N/A";}else{diedCityVal=diedCity;
                    diedCountryVal = diedCountry;diedCountryCodeVal= diedCountryCode;}
        
        if (born.equals("0000-00-00")) {bornVal = "N/A";} else{bornVal = born;}
        if (bornCity == null || bornCountry == null || bornCountryCode == null) {
            bornCityVal= "N/A"; bornCountryVal="N/A"; bornCountryCodeVal="N/A";} else{bornCityVal= bornCity;
                    bornCountryVal = bornCountry; bornCountryCodeVal= bornCountryCode;}
        
        return "\n"+
               "Name: "+nameVal+"\n"+
               "Gender: "+gender+"\n"+
               "Age: "+ageVal+"\n"+
               "Born: "+bornVal+"\n"+
               bornCityVal+", "+bornCountryVal+", "+bornCountryCodeVal+"\n"+
               "\n"+
               "Died: "+diedVal+"\n"+
               diedCityVal+", "+diedCountryVal+", "+diedCountryCodeVal+"\n"+
               "\n"+
               "Prizes"+Prizes;
        
    }
}
