package nobellaureates;

import java.text.Normalizer;
import java.time.Year;
import java.util.Collections;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.transformation.FilteredList;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;

/**
 * Handles interaction between data and display as well as input / filter
 * management
 * @author Declan Simkins
 */
public class SearchListController {
    private final LaureateDisplay lauDisplay;
    private final SearchListDisplay listDisplay;
    private final FilterDisplay filterDisplay;
    private final Model model;
    
    /**
     * Setups, populates displays and establishes filter bindings
     * @param init_model data model to be used for UI population
     * @param init_listDisplay laureate list display to be used
     * @param init_lauDisplay laureate info display to be used
     * @param init_filterDisplay filter field / choice box display to be used
     **/
    public SearchListController(Model init_model, Display init_listDisplay,
                                Display init_lauDisplay, Display init_filterDisplay) {
        
        this.model = init_model;
        this.filterDisplay = (FilterDisplay) init_filterDisplay;
        this.lauDisplay = (LaureateDisplay) init_lauDisplay;
        this.listDisplay = (SearchListDisplay) init_listDisplay;
        this.searchHelper();
        this.laureateListSetup(listDisplay.getList());
        this.populateUI();
    }
    
    /**
     * Setsup cellfactory for given listView
     * Establishes event handling for cell being selected (displays laureate
     * contained within cell's info)
     * @param list ListView to be setup
     **/
    private void laureateListSetup(ListView list) {
        list.setCellFactory(new Callback<ListView<Laureate>, ListCell<Laureate>>() {
            //change the items in the list view from array [id:"Albert Einstien",,,] to just the laureates name
            @Override
            public ListCell<Laureate> call(ListView<Laureate> param) {
                ListCell<Laureate> cell = new ListCell<Laureate>() {
                    @Override
                    protected void updateItem(Laureate item, boolean empty) {
                        super.updateItem(item, empty);
                        if(item != null) {
                            setText(item.getFullName());
                        } else {
                            setText(null);
                        }
                    }
                };
                //if you click something in the listview
                cell.setOnMouseClicked((MouseEvent event) -> {
                    if ( cell.getItem() != null ) {
                        if (!(lauDisplay.getRoot().isVisible()))
                            lauDisplay.getRoot().setVisible(true);
                        lauDisplay.setText(cell.getItem().toString());
                        lauDisplay.setImage(getImage(cell.getItem()));
                    }
                });
                return cell;
            }
        });
    }
    
    /**
     * Gets the image associated with a given laureate
     * @param lau Laureate who's image will be gotten
     * @return Image of laureate
     **/
    public Image getImage(Laureate lau) {
        String surname = " ";
        String category;
        //I did some research online and found out about this pattern is a regex that matches
        //all diacritic symbols
        Pattern NormalizeText = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        if(lau.getSurname() != null){
            String str = lau.getSurname().toLowerCase();
            //Check if the surname contains multiple words, and if so handle the
            //cases with jr and the cases with a comma
            if(str.contains(" ")){
                if(str.contains("jr.")){
                    str = str.substring(0,str.indexOf("jr."));
                    //for the cases with jr and a comma, then remove the comma
                    if(str.contains(",")){
                        str=str.substring(0,str.indexOf(","));
                    }
                }
                else{
                    //in caseswith a comma, get only the name before the comma
                    if(str.contains(",")){
                        str=str.substring(0,str.indexOf(","));
                    }
                    //Otherwise, get the very last name
                    str = str.substring(str.lastIndexOf(" ")+1);
                }
            }
            //Get rid of all empty spaces and apostrophes
            str = str.replaceAll("'", "").replaceAll(" ", "");
            //Now we replace all the diacritic symbols, if any
            surname = Normalizer.normalize(str, Normalizer.Form.NFD);
            surname = NormalizeText.matcher(surname).replaceAll("");
        }
        if(lau.getPrizes().get(0).getCategory() == null){
            System.out.println("NO IMAGE");
        }
        if(lau.getPrizes().get(0).getCategory().equals("economics")){
            category = "economic-sciences";
        }
        else{
            category = lau.getPrizes().get(0).getCategory();
        }
        if(lau.getPrizes().get(0).getYear() == null){
            System.out.println("NO IMAGE");
        }
        String year = lau.getPrizes().get(0).getYear();
        String PicURL = "https://www.nobelprize.org/nobel_prizes/"+category+"/laureates/"+year+"/"+surname+"_postcard.jpg";
        Image image = new Image(PicURL);
        return image;
    }
    
    /**
     * Internal helper function: populates filters
     **/
    private void populateUI() {
        this.filterDisplay.getCountryBox().getItems().addAll(this.model.getCountryList());
        this.filterDisplay.getCategoryBox().getItems().addAll(this.model.getCategoryList());
        this.filterDisplay.getGenderBox().getItems().addAll(this.model.getGenderList());
        
        int minYear = this.getMinYear();
        int maxYear = Year.now().getValue();
        this.filterDisplay.getPrizeMinSlider().setMin(minYear);
        this.filterDisplay.getPrizeMinSlider().setMax(maxYear);
        this.filterDisplay.getPrizeMaxSlider().setMin(minYear);
        this.filterDisplay.getPrizeMaxSlider().setMax(maxYear);
        this.filterDisplay.getPrizeMaxSlider().setValue(maxYear);
    }
    
    /**
     * Internal helper function: gets smallest year number from this controller's
     * data model
     * @return smallest year as int
     **/
    private int getMinYear() {
        int min = Year.now().getValue();
        for (Laureate l : this.model.getLaureateList()) {
            for (Prize p : l.getPrizes()) {
                if (p.getYear() != null && Integer.parseInt(p.getYear()) < min)
                    min = Integer.parseInt(p.getYear());
            }
        }
        return min;
    }
    
    /**
     * Establishes filter bindings to UI elements
     **/
    private void searchHelper() {
        ObjectProperty<Predicate<Laureate>> countryFilter = new SimpleObjectProperty<>();
        ObjectProperty<Predicate<Laureate>> genderFilter = new SimpleObjectProperty<>();
        ObjectProperty<Predicate<Laureate>> diedCountryFilter = new SimpleObjectProperty<>();
        ObjectProperty<Predicate<Laureate>> cityFilter = new SimpleObjectProperty<>();
        ObjectProperty<Predicate<Laureate>> categoryFilter = new SimpleObjectProperty<>();
        ObjectProperty<Predicate<Laureate>> textInputFilter = new SimpleObjectProperty<>();
        ObjectProperty<Predicate<Laureate>> yearBornInputFilter = new SimpleObjectProperty<>();
        ObjectProperty<Predicate<Laureate>> yearDiedInputFilter = new SimpleObjectProperty<>();
        ObjectProperty<Predicate<Laureate>> firstInputFilter = new SimpleObjectProperty<>();
        ObjectProperty<Predicate<Laureate>> PrizeYearInputFilter = new SimpleObjectProperty<>();
        ObjectProperty<Predicate<Laureate>> AgeInputFilter = new SimpleObjectProperty<>();
        ObjectProperty<Predicate<Laureate>> PrizeMinFilter = new SimpleObjectProperty<>();
        ObjectProperty<Predicate<Laureate>> PrizeMaxFilter = new SimpleObjectProperty<>();
        
        FilteredList <Laureate> filteredItems = new FilteredList<>(model.getLaureates());
        
        // Prize year slider filter (min)
        PrizeMinFilter.bind(Bindings.createObjectBinding(() -> (Laureate lau)
            -> filterDisplay.getPrizeMinSlider().getValue() <= Integer.parseInt
            (Collections.max(lau.getPrizes()).getYear())
            , filterDisplay.getPrizeMinSlider().valueProperty()));
        
        // Prize year slider filter (max)
        PrizeMaxFilter.bind(Bindings.createObjectBinding(() -> (Laureate lau)
            -> filterDisplay.getPrizeMaxSlider().getValue() >= Integer.parseInt
            (Collections.min(lau.getPrizes()).getYear())
            , filterDisplay.getPrizeMaxSlider().valueProperty()));
        
        // Contain-style filter for country names
        countryFilter.bind(Bindings.createObjectBinding(() -> (Laureate lau) 
            -> filterDisplay.getCountryBox().getValue() == null
            || filterDisplay.getCountryBox().getValue().equals("-- Country --")
            || filterDisplay.getCountryBox().getValue().equals(lau.getBornCountry())
            , filterDisplay.getCountryBox().valueProperty()));
        
        // Contain-style filter for gender
        genderFilter.bind(Bindings.createObjectBinding(() -> (Laureate lau)
            -> filterDisplay.getGenderBox().getValue() == null
            || filterDisplay.getGenderBox().getValue().equals("-- Gender --")
            || (filterDisplay.getGenderBox().getValue().equals(lau.getGender().toLowerCase()))
            , filterDisplay.getGenderBox().valueProperty()));
        
        // Contain-style filter for category
        categoryFilter.bind(Bindings.createObjectBinding(() -> (Laureate lau)
            -> filterDisplay.getCategoryBox().getValue() == null
            || filterDisplay.getCategoryBox().getValue().equals("-- Category --")
            || (filterDisplay.getCategoryBox().getValue().equals(lau.getPrizes().get(0).getCategory()))
            , filterDisplay.getCategoryBox().valueProperty()));
        
        // Contain-style filter for name search
        textInputFilter.bind(Bindings.createObjectBinding(() -> (Laureate lau)
            -> listDisplay.getSearch().getText() == null ||
            (lau.getFullName().toLowerCase().contains(listDisplay.getSearch().getText().toLowerCase()))
            , listDisplay.getSearch().textProperty()));
        
        // Filter for born text field
        // Compares each number positionally to the laureate's year born
        // Returns true if the value in the text field matches the same length
        //   sub string (from 0th position) of the laureate's data
        yearBornInputFilter.bind(Bindings.createObjectBinding(() -> new Predicate<Laureate>() {
            @Override
            public boolean test(Laureate lau) {
                int matches = 0;
                for (int i = 0; i < filterDisplay.getBornField().getText().length(); i++) {
                    if (filterDisplay.getBornField().getText().charAt(i) == lau.getBorn().charAt(i))
                        matches++;
                }
                return matches == filterDisplay.getBornField().getText().length();
            }
        }, filterDisplay.getBornField().textProperty()));
        
        
        // Filter for died text field
        // Compares each number positionally to the laureate's year died
        // Returns true if the value in the text field matches the same length
        //   sub string (from 0th position) of the laureate's data
        yearDiedInputFilter.bind(Bindings.createObjectBinding(() -> new Predicate<Laureate>() {
            @Override
            public boolean test(Laureate lau) {
                int matches = 0;
                for (int i = 0; i < filterDisplay.getDiedField().getText().length(); i++) {
                    if (filterDisplay.getDiedField().getText().charAt(i) == lau.getDied().charAt(i))
                        matches++;
                }
                return matches == filterDisplay.getDiedField().getText().length();
            }
        }, filterDisplay.getDiedField().textProperty()));
        
        // Filter for age text field
        // Compares each number positionally to the laureate's age
        // Returns true if the value in the text field matches the same length
        //   sub string (from 0th position) of the laureate's data
        AgeInputFilter.bind(Bindings.createObjectBinding(() -> new Predicate<Laureate>() {
            @Override
            public boolean test(Laureate lau) {
                if (filterDisplay.getAgeField().getText().isEmpty())
                    return true;
                if (lau.getGender().equals("org"))
                    return false;
                if (lau.getAge() == 0)
                    return true;
                
                int matches = 0;
                    for (int i = 0; i < filterDisplay.getAgeField().getText().length() && i < String.valueOf(lau.getAge()).length(); i++) {
                        if (filterDisplay.getAgeField().getText().charAt(i) == String.valueOf(lau.getAge()).charAt(i))
                            matches++;
                    }
                return matches == filterDisplay.getAgeField().getLength();
            }
        }, filterDisplay.getAgeField().textProperty()));
        
        // Filter for prize year text field
        // Compares each number positionally to each of the laureate's prizes
        // Returns true if the value in the text field matches the same length
        //   sub string (from 0th position) of the laureate's data
        PrizeYearInputFilter.bind(Bindings.createObjectBinding(() -> new Predicate<Laureate>() {
            @Override
            public boolean test(Laureate lau) {
                int matches = 0;
                for (Prize p : lau.getPrizes()) {
                    matches = 0;
                    for (int i = 0; i < filterDisplay.getPrizeField().getText().length(); i++) {
                        if (filterDisplay.getPrizeField().getText().charAt(i) == p.getYear().charAt(i))
                            matches++;
                    }
                    if (matches == filterDisplay.getPrizeField().getText().length())
                        return true;
                }
                return false;
            }
        }, filterDisplay.getPrizeField().textProperty()));
        
        // Adds all filters to the list of laureates used by the ListView
        this.listDisplay.getList().setItems(filteredItems);
        filteredItems.predicateProperty().bind(Bindings.createObjectBinding(
                () -> countryFilter.get().and(genderFilter.get().and(textInputFilter.get()).and(yearBornInputFilter.get()).and(yearDiedInputFilter.get()).and(categoryFilter.get()).and(PrizeYearInputFilter.get()).and(AgeInputFilter.get()).and(PrizeMinFilter.get()).and(PrizeMaxFilter.get())), 
                countryFilter, genderFilter, textInputFilter, yearBornInputFilter, yearDiedInputFilter, categoryFilter, PrizeYearInputFilter, AgeInputFilter, PrizeMinFilter, PrizeMaxFilter));
    }
}
