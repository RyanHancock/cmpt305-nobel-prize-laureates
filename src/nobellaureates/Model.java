package nobellaureates;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;

/**
 * Holds all data needed by the display and controller
 * @author Declan Simkins
 */
public class Model {
    private FilteredList <String> genderList, countryList, categoryList;
    private final ObservableList <Laureate> Laureates;
    private final ArrayList <Laureate> laureateList;
    
    /**
     * Gets and sorts info from the web reader, then populates the display
     * choice boxes
     **/
    public Model() {
        WebReader reader = new WebReader();
        this.laureateList = reader.getLaureates();
        Collections.sort(this.laureateList);
        this.Laureates = FXCollections.observableArrayList(this.laureateList);
        initChoiceBoxLists();
    }
    
    /**
     * Initialize country, category, and gender boxes with sorted info
     **/
    private void initChoiceBoxLists() {
        List <String> tempCategoryList, tempGenderList, tempCountryList;
        tempCategoryList = new ArrayList<>();
        tempGenderList = new ArrayList<>();
        tempCountryList = new ArrayList<>();
        
        for (Laureate l : this.Laureates) {
            String gender = l.getGender();
            String country = l.getBornCountry();
            String category = l.getPrizes().get(0).getCategory();
            
            if(!tempCategoryList.contains(category) && category != null)
                tempCategoryList.add(category);
            if(!tempGenderList.contains(gender) && gender != null)
                tempGenderList.add(gender);
            if(!tempCountryList.contains(country) && country != null)
                tempCountryList.add(country);
        }
        
        Collections.sort(tempGenderList);
        Collections.sort(tempCountryList);
        Collections.sort(tempCategoryList);
        this.categoryList = new FilteredList(FXCollections.observableArrayList(tempCategoryList));
        this.countryList = new FilteredList(FXCollections.observableArrayList(tempCountryList));
        this.genderList = new FilteredList(FXCollections.observableArrayList(tempGenderList));
    }
    
    // Get Methods
    public ArrayList <Laureate> getLaureateList() { return this.laureateList; }
    public ObservableList <Laureate> getLaureates() { return this.Laureates; }
    public FilteredList <String> getCategoryList() { return this.categoryList; }
    public FilteredList <String> getCountryList() { return this.countryList; }
    public FilteredList <String> getGenderList() { return this.genderList; }
}
