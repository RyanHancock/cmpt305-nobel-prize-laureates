package nobellaureates;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Gets laureate data from the web
 * @author Declan Simkins
 */
public class WebReader {
    private ArrayList <Laureate> Laureates;
    
    public WebReader() {
        this.Laureates = new ArrayList<>();
        this.GetLaureateData();
    }
    
    /**
     * Retrieves all laureate data from nobelprize.org
     **/
    private void GetLaureateData(){
        try{
            URL Laureate = new URL("http://api.nobelprize.org/v1/laureate.json");
            try (BufferedReader in = new BufferedReader(new InputStreamReader(Laureate.openStream()))) {
                String inputLine;
                while ((inputLine=in.readLine())!=null){
                    JSONObject obj = new JSONObject(inputLine);
                    JSONArray laureates = obj.getJSONArray("laureates");
                    int entries= laureates.length();
                    for(int i=0;i<entries;i++){
                        Laureate newLau =new Laureate();
                        String ID = laureates.getJSONObject(i).getString("id");
                        newLau.setId(ID);
                        if(!laureates.getJSONObject(i).has("firstname")){
                            continue;
                        }
                        newLau.setFirstname(laureates.getJSONObject(i).getString("firstname"));
                        if(laureates.getJSONObject(i).has("surname")){
                            newLau.setSurname(laureates.getJSONObject(i).getString("surname"));
                        }
                        if(laureates.getJSONObject(i).has("born")){
                            newLau.setBorn(laureates.getJSONObject(i).getString("born"));
                        }
                        if(laureates.getJSONObject(i).has("died")){
                            newLau.setDied(laureates.getJSONObject(i).getString("died"));
                        }
                        if(newLau.getDied().substring(0,4).equals("0000") && newLau.getBorn().substring(0,4).equals("0000")){
                            newLau.setAge(0);
                        }
                        else{
                            if(newLau.getBorn() != null && newLau.getDied() != null){
                            newLau.setAge(Integer.parseInt(newLau.getDied().substring(0,4)) - Integer.parseInt(newLau.getBorn().substring(0,4)));
                            }
                            if((newLau.getBorn() != null && newLau.getDied() == null) || (newLau.getDied().substring(0,4).equals("0000") && newLau.getBorn() != null)){
                                newLau.setAge(2017 - Integer.parseInt(newLau.getBorn().substring(0,4)));
                            }
                        }
                        if(laureates.getJSONObject(i).has("bornCountry")){
                            newLau.setBornCountry(laureates.getJSONObject(i).getString("bornCountry"));
                        }
                        if(laureates.getJSONObject(i).has("bornCountryCode")){
                            newLau.setBornCountryCode(laureates.getJSONObject(i).getString("bornCountryCode"));
                        }
                        if(laureates.getJSONObject(i).has("bornCity")){
                            newLau.setBornCity(laureates.getJSONObject(i).getString("bornCity"));
                        }
                        if(laureates.getJSONObject(i).has("diedCountry")){
                            newLau.setDiedCountry(laureates.getJSONObject(i).getString("diedCountry"));
                        }
                        if(laureates.getJSONObject(i).has("diedCountryCode")){
                            newLau.setDiedCountryCode(laureates.getJSONObject(i).getString("diedCountryCode"));
                        }
                        if(laureates.getJSONObject(i).has("diedCity")){
                            newLau.setDiedCity(laureates.getJSONObject(i).getString("diedCity"));
                        }
                        if(laureates.getJSONObject(i).has("gender")){
                            newLau.setGender(laureates.getJSONObject(i).getString("gender"));
                        }
                        int PrizesLength= laureates.getJSONObject(i).getJSONArray("prizes").length();
                        JSONArray prizes = laureates.getJSONObject(i).getJSONArray("prizes");
                        for(int j=0;j<PrizesLength;j++){
                            Prize newPrize = new Prize();
                            if(prizes.getJSONObject(j).has("year")){
                                newPrize.setYear(prizes.getJSONObject(j).getString("year"));
                            }
                            if(prizes.getJSONObject(j).has("category")){
                                newPrize.setCategory(prizes.getJSONObject(j).getString("category"));
                            }
                            if(prizes.getJSONObject(j).has("share")){
                                newPrize.setShare(prizes.getJSONObject(j).getString("share"));
                            }
                            if(prizes.getJSONObject(j).has("motivation")){
                                newPrize.setMotivation(prizes.getJSONObject(j).getString("motivation"));
                            }
                            JSONArray affiliates= prizes.getJSONObject(j).getJSONArray("affiliations");
                            //String afflen = affiliates.get(0).substring(1,affiliates.length());
                            if(affiliates.get(0).getClass().getName().equals("org.json.JSONObject")){
                                Affiliations newAff = new Affiliations();
                                if(affiliates.getJSONObject(0).has("name")){
                                    newAff.setName(affiliates.getJSONObject(0).getString("name"));
                                }
                                if(affiliates.getJSONObject(0).has("diedCity")){
                                    newAff.setCity(affiliates.getJSONObject(0).getString("city"));
                                }
                                if(affiliates.getJSONObject(0).has("gender")){
                                    newAff.setCountry(affiliates.getJSONObject(0).getString("country"));
                                }
                                newPrize.setAffiliation(newAff);
                            }
                            else{
                                Affiliations newAff = new Affiliations();
                                newAff.setName("N/A");
                                newAff.setCity("N/A");
                                newAff.setCountry("N/A");
                                newPrize.setAffiliation(newAff);
                            }
                            newLau.addPrize(newPrize);
                        }
                        Laureates.add(newLau);
                    }
                }
            }
        } catch (MalformedURLException ex) {
            Logger.getLogger(Model.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Model.class.getName()).log(Level.SEVERE, null, ex);
        }  
    }
    
    public ArrayList <Laureate> getLaureates() { return this.Laureates; }
}
