/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nobellaureates;

import java.util.ArrayList;

/**
 * Holds all data for prizes
 * @author ryanhancock
 */
public class Prize implements Comparable {
    private String category, year, share, motivation;
    private ArrayList<Laureate> sharewith = new ArrayList<>();
    private Affiliations Affiliation;
    //SHOULD REMOVE THIS AND MAKE IT SO THAT PRIZES ARE A PART OF LAUREATE
    //NOT LAUREATE A PART OF PRIZES
    //THIS WOULD HELP THE SEARCHING SO MUCH, AND JUST MAKES MORE SENSE
    
    @Override
    public int compareTo(Object o) {
        if (!(o instanceof Prize))
            throw new IllegalArgumentException();
        return Integer.parseInt(this.getYear()) - Integer.parseInt(((Prize) o).getYear());
    }
    
    // Get methods
    public Affiliations getAffiliation() { return Affiliation; }
    public String getCategory () { return category; }
    public String getYear () { return year; }
    public String getMotivation(String mot) { return motivation; }
    public ArrayList<Laureate> getSharewith() { return sharewith; }
    public String getShare(String share) { return share; }
    
    // Set methods
    public void setShare(String share) { this.share = share; }
    public void setMotivation(String mot) { this.motivation = mot; }
    public void setSharewith(ArrayList<Laureate> sharewith) { this.sharewith = sharewith; }
    public void setYear (String year) { this.year = year; }
    public void setCategory (String category) { this.category = category; }
    public void setAffiliation(Affiliations Affiliation) { this.Affiliation = Affiliation; }
    
    public void addSharewith(Laureate lau) { sharewith.add(lau); }
    
    @Override
    public String toString()
    {
        String motivationVal;
        if(motivation == null){motivationVal = "N/A";}else{motivationVal = motivation;}
        
        return "\n"+
               "\n"+
               "Year: "+year+"\n"+
               "Category: "+category+"\n"+
               "Motivation: "+motivationVal+"\n"+
               "Share: "+"1/"+share+"\n"+
               "\n"+
               "Affiliation"+Affiliation;
    }
}
