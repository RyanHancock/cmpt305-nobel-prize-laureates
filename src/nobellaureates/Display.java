package nobellaureates;

import java.util.LinkedList;
import java.util.Observable;
import java.util.Observer;
import javafx.event.Event;
import javafx.scene.Node;

/**
 * Abstract class detailing abstract functions and implementing unchanging
 * ones. Also provides a default constructor for subclasses
 * @author Declan Simkins
 */
public abstract class Display extends Observable {
    protected final LinkedList <Observer> observers;
    
    public Display() {
        this.observers = new LinkedList<>();
    }
    
    public abstract Node getRoot();
    public void notify(Event e) {
        for (Observer o : this.observers) {
            o.update(this, e);
        }
    }
    
    public void removeObserver(Observer o) {
        this.observers.remove(o);
    }
    
    public void addListener(Observer o) {
        this.observers.add(o);
    }
}
