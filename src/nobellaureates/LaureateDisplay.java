package nobellaureates;

import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Text;

/**
 * Display for all Laureate info and image
 * All UI elements which need to be modified are kept track of as object fields
 * and are made publicly accessible through their respective get functions to
 * allow for modification of their values through other parts of the program
 * @author Declan Simkins
 */
public class LaureateDisplay extends Display {
    private final ScrollPane root;
    private final HBox infoBox;
    private final ImageView lauImage;
    private final Text lauText;
    
    /**
     * Creates and places all UI elements within a scroll pane
     * Population of elements takes within the controller
     **/
    public LaureateDisplay() {
        // Create / setup main scroll pane
        this.root = new ScrollPane();
        this.root.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        this.root.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        this.root.setFitToWidth(true);
        this.root.setFitToHeight(true);
        this.root.setVisible(false);
        
        // Create horizontal stack box for laureate info / image
        this.infoBox = new HBox();
        this.infoBox.setSpacing(10);
        
        // Imageview and text box for laureate info
        this.lauImage = new ImageView();
        this.lauText = new Text();
        this.lauText.setFill(Color.WHITE);
        // Wrap laureate info text at approximate edge of hbox
        this.lauText.wrappingWidthProperty().bind(root.widthProperty().divide(2).subtract(15));
        
        // Add laureate image view and text to hbox
        this.infoBox.getChildren().addAll(lauImage, lauText);
        // Add laureate info hbox to scroll pane
        this.root.setContent(infoBox);
    }
    
    @Override
    public Node getRoot() { return this.root; }
    // Get Methods
    public HBox getInfoBox() { return this.infoBox; }
    public ImageView getImage() { return this.lauImage; }
    public Text getText() { return this.lauText; }
    
    // Set Methods
    public void setImage(Image img) { this.lauImage.setImage(img); }
    public void setText(String txt) { this.lauText.setText(txt); }
}
