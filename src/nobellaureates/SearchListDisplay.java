package nobellaureates;

import javafx.collections.transformation.FilteredList;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

/**
 * Display for list of laureates and name search field
 * Tracks the search field as an object field to allow for public access through
 * simple getter
 * @author Declan Simkins
 */
public class SearchListDisplay extends Display {
    private GridPane root;
    private ListView list;
    private TextField search;
    
    public SearchListDisplay() {
        // Create / setup main grid
        this.root = new GridPane();
        this.root.setPadding(new Insets(3,3,3,3));
        this.root.setVgap(10);
        
        // Create / setup listview for laureates
        this.list = new ListView();
        // Keep width at same width as container
        this.list.prefWidthProperty().bind(this.root.widthProperty());
        this.list.setMinHeight(450);
        // Keep height at same height as container
        this.list.prefHeightProperty().bind(this.root.heightProperty());
        this.search = new TextField();
        this.search.setPromptText("Name");
        
        // Add nodes to main grid
        root.add(this.search, 0, 0);
        root.add(this.list, 0, 1);
    }
    
    @Override
    public Node getRoot() { return this.root; }
    public ListView getList() { return this.list; }
    public TextField getSearch() { return this.search; }
    
    /**
     * Sets list view elements
     * @param obsvList a filtered (observable) list to assign to the display's
     * list view
     **/
    public void setList(FilteredList <Laureate> obsvList) {
        this.list.setItems(obsvList);
    }
}
