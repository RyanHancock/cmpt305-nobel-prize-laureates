package nobellaureates;

/**
 * Stores data for Laureate affiliates
 * @author Ryan Hancock
 */
public class Affiliations {
    public String name;
    public String city;
    public String country;
    
    // Get Methods
    public String getName() { return this.name; }
    public String getCity() { return this.city; }
    public String getCountry() { return this.country; }
    
    // Set Methods
    public void setCity(String city) { this.city = city; }
    public void setName(String name) { this.name = name; }
    public void setCountry(String country) { this.country = country; }
    
    @Override
    public String toString()
    {
        String nameVal, cityVal, countryVal;
        if(name == null) {nameVal = "N/A";} else{nameVal = name;}
        if(city == null) {cityVal = "N/A";} else{cityVal = city;}
        if(country == null) {countryVal = "N/A";} else{countryVal = country;}
        
        
        return "\n"+
               "\n"+
               "Name: " + nameVal + "\n" +
               "City: " + cityVal + "\n" +
               "Country: " + countryVal;
    }
}
