package nobellaureates;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Runs application and populates / sets up the main window
 * Also gets css sheet
 * @author Declan Simkins
 */
public class NobelLaureates extends Application {
    
    @Override
    public void start(Stage primaryStage) {
        // Create / setup window
        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Nobel Prize Laureates");
        window.setMinWidth(800);
        window.setMinHeight(600);
        
        // Create / setup main gridpane
        GridPane root = new GridPane();
        root.setPadding(new Insets(10,10,10,10));
        root.setHgap(10);
        // Set 1st column to be 30% the width of the grid pane
        ColumnConstraints searchListColumn = new ColumnConstraints();
        searchListColumn.setPercentWidth(30);
        root.getColumnConstraints().add(searchListColumn);
        
        // Vertical stack box for filters and laureate info
        VBox filterLauBox = new VBox();
        filterLauBox.setSpacing(10);
        
        // Initialize main displays
        FilterDisplay fdisplay = new FilterDisplay();
        SearchListDisplay sldisplay = new SearchListDisplay();
        LaureateDisplay ldisplay = new LaureateDisplay();
        // Initialize data model and controller
        Model model = new Model();
        SearchListController slcontrol = new SearchListController
            (model, sldisplay, ldisplay, fdisplay);
        
        // Add nodes to filter and laureate info vbox
        filterLauBox.getChildren().add(fdisplay.getRoot());
        filterLauBox.getChildren().add(ldisplay.getRoot());
        
        // Add nodes to main grid pane
        root.add(sldisplay.getRoot(), 0, 0);
        root.add(filterLauBox, 1, 0);
        
        // Set scene and get apply css
        Scene mainFrame = new Scene(root, 800, 500);
        mainFrame.getStylesheets().add("nobellaureates/style.css");
        
        // Display window
        window.setScene(mainFrame);
        window.show();
    }

    /**
     * @param args Command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
