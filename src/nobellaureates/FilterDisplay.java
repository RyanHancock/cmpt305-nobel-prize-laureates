package nobellaureates;

import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

/**
 * Display for all filter boxes and filter search fields
 * All UI elements which act as filters are kept track of as object fields and
 * are made publicly accessible through their respective get functions to allow
 * for binding and access of their values through other parts of the program
 * @author Declan Simkins
 */
public class FilterDisplay extends Display{
    private final TextField ageField, prizeField, bornField, diedField;
    private final ChoiceBox countryBox, genderBox, categoryBox;
    private final Slider prizeYearMin, prizeYearMax;
    private final GridPane root;
    
    /**
     * Creates and places all UI elements within a grid pane
     * Population of elements takes place within the controller
     **/
    public FilterDisplay() {
        super();
        
        // Create main grid pane
        this.root = new GridPane();
        this.root.setPadding(new Insets(3,3,3,3));
        this.root.setHgap(10);
        this.root.setVgap(5);
        
        // Create grid pane for choice boxes and text field
        GridPane filterGrid = new GridPane();
        filterGrid.setHgap(10);
        filterGrid.setVgap(5);
        
        // Create age and prizeYear text fields for filtering
        this.ageField = new TextField();
        this.ageField.setPromptText("Age");
        this.prizeField = new TextField();
        this.prizeField.setPromptText("Prize Year");
        
        // Create horizontal stack box for year born / died text fields
        HBox bornDiedHStack = new HBox();
        bornDiedHStack.setSpacing(10);
        
        // Create year born / died text fields for filtering
        this.bornField = new TextField();
        this.bornField.setPromptText("Year Born");
        this.diedField = new TextField();
        this.diedField.setPromptText("Year Died");
        
        // Add born / died text fields to horizontal stack box
        bornDiedHStack.getChildren().addAll(bornField, diedField);
        
        // Create country list choice box
        this.countryBox = new ChoiceBox();
        this.countryBox.prefWidthProperty().bind(root.widthProperty());
        this.countryBox.getItems().add("-- Country --");
        this.countryBox.getSelectionModel().selectFirst();
        
        // Create gender list choice box
        this.genderBox = new ChoiceBox();
        this.genderBox.prefWidthProperty().bind(root.widthProperty());
        this.genderBox.getItems().add("-- Gender --");
        this.genderBox.getSelectionModel().selectFirst();
        
        // Create prize category list choice box
        this.categoryBox = new ChoiceBox();
        this.categoryBox.prefWidthProperty().bind(root.widthProperty());
        this.categoryBox.getItems().add("-- Category --");
        this.categoryBox.getSelectionModel().selectFirst();
        
        // Create horizontal stack box for prize year sliders
        HBox sliderHBox = new HBox();
        sliderHBox.setSpacing(20);
        
        // Title for prize year sliders
        Text prizeSliderTitle = new Text("Prize Year:");
        prizeSliderTitle.setFill(Color.WHITE);
        
        // Create vertical stack box for prize year min slider and its title
        VBox prizeMinVBox = new VBox();
        prizeMinVBox.setSpacing(5);
        
        // Create / setup prize year min slider and title
        Text prizeMinTitle = new Text("From:");
        prizeMinTitle.setFill(Color.WHITE);
        this.prizeYearMin = new Slider();
        this.prizeYearMin.setShowTickLabels(true);
        this.prizeYearMin.setShowTickMarks(true);
        this.prizeYearMin.setMajorTickUnit(30);
        this.prizeYearMin.setMinorTickCount(3);
        this.prizeYearMin.setBlockIncrement(20);
        this.prizeYearMin.setMinWidth(200);
        
        // Add prize year min slider and title to vertical stack box
        prizeMinVBox.getChildren().addAll(prizeMinTitle, prizeYearMin);
        
        // Create vertical stack box for prize year max slider and its title
        VBox prizeMaxVBox = new VBox();
        prizeMaxVBox.setSpacing(5);
        
        // Create / setup prize year max slider and title
        Text prizeMaxTitle = new Text("To:");
        prizeMaxTitle.setFill(Color.WHITE);
        this.prizeYearMax = new Slider();
        this.prizeYearMax.setShowTickLabels(true);
        this.prizeYearMax.setShowTickMarks(true);
        this.prizeYearMax.setMajorTickUnit(30);
        this.prizeYearMax.setMinorTickCount(3);
        this.prizeYearMax.setBlockIncrement(20);
        this.prizeYearMax.setMinWidth(200);
        
        // Add prize year max slider and title to vertical stack box
        prizeMaxVBox.getChildren().addAll(prizeMaxTitle, prizeYearMax);
        
        // Add prize year min / max vboxes to slider hbox
        sliderHBox.getChildren().addAll(prizeSliderTitle, prizeMinVBox, prizeMaxVBox);
        
        // Add all filter elements to grid
        filterGrid.add(ageField, 0, 0);
        filterGrid.add(genderBox, 0, 1);
        filterGrid.add(bornDiedHStack, 1, 0);
        filterGrid.add(countryBox, 1, 1);
        filterGrid.add(prizeField, 2, 0);
        filterGrid.add(categoryBox, 2, 1);
        
        // Add filter grid and slider hbox to main grid
        this.root.add(filterGrid, 0, 0);
        this.root.add(sliderHBox, 0, 1);
    }
    
    @Override
    public Node getRoot() { return this.root; }
    
    // Get methods
    public TextField getAgeField() { return this.ageField; }
    public TextField getBornField() { return this.bornField; }
    public TextField getDiedField() { return this.diedField; }
    public TextField getPrizeField() { return this.prizeField; }
    public ChoiceBox getCountryBox() { return this.countryBox; }
    public ChoiceBox getCategoryBox() { return this.categoryBox; }
    public ChoiceBox getGenderBox() { return this.genderBox; }
    public Slider getPrizeMaxSlider() { return this.prizeYearMax; }
    public Slider getPrizeMinSlider() { return this.prizeYearMin; }
}
